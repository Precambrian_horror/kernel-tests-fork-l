# storage/ssd/nvme_driver_null_pointer_with_reboot_bz1422521

Storage: NVMe driver null pointer with reboot stress test 

## How to run it
Please refer to the top-level README.md for common dependencies.

### Install dependencies
```bash
root# bash ../../../cki_bin/pkgs_install.sh metadata
```

### Execute the test
```bash
bash ./runtest.sh
```
